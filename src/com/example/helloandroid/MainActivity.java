package com.example.helloandroid;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.*;
import android.net.Uri;
import android.view.*;
import android.*;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.*; //untuk import TextView dan lain lain

public class MainActivity extends Activity {

    static {
        //System.loadLibrary("gmath");
        //System.loadLibrary("gperf");
        System.loadLibrary("hello");
    }

    public native String getMessage();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn = (Button) findViewById(R.id.buttonid1); 
        TextView textView = (TextView) findViewById(R.id.textview1); 

        //int count = 1; 
        textView.setText(getMessage()); 

        // btn.setOnClickListener( new OnClickListener() { 
        //     @Override public void onClick(View v) { 
        //         textView.setText("this is text " ); 
        //     } 
        // });

        WebView webView = (WebView) findViewById(R.id.webview);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);

        // Tiga baris di bawah ini agar laman yang dimuat dapat
        // melakukan zoom.
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        // Baris di bawah untuk menambahkan scrollbar di dalam WebView-nya
        // webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.setWebViewClient(new WebViewClient());
        // webView.loadUrl("https://www.codepolitan.com");
        // webView.loadUrl(Uri.parse("file:///android_assets/index.html").toString());
        webView.loadUrl("file:///android_asset/index.html");
    }
}