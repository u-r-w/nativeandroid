#include <jni.h>
#include "HelloJNI.h"
 
JNIEXPORT jstring JNICALL Java_com_example_helloandroid_MainActivity_getMessage
          (JNIEnv *env, jobject thisObj) {
   return (*env)->NewStringUTF(env, "Hello mark from native code");
}